// controllers contain the function and business logic of our express js application
// meaning all the operations it can do will be placed in this file
const task = require("../models/task")
const Task=require("../models/task")
// controller function for getting all the task
module.exports.getAllTasks=()=>{
    return Task.find({}).then(result=>{
        return result
    })
}
// controller function for creating a task
module.exports.createTask=(requestBody)=>{
    // creates a task obj based on the mongoose model "Task"
    let newTask=new Task({
        name: requestBody.name
    })

// save the newly created "newTask" obj in the mongoDB database
return newTask.save().then((task,error)=>{
    if(error){
        console.log(error)
        return false;
    }else{
        return task;
    }
})}
// creating a controller function for deleting a task
// "taskID" is the URL parameter passed from the "taskRoute.js"
module.exports.deleteTask=(taskId)=>{
    return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
        if(err){
            console.log(err)
            return false;
        }else{
            return removedTask
        }
    })
}
// controller function updating a task
module.exports.updateTask=(taskId, newContent)=>{
    return Task.findById(taskId).then((result,error)=>{
        if(error){
            console.log(error)
            return false
        }
        result.name=newContent.name;
        return result.save().then((updatedTask,saveErr)=>{
            if(saveErr){
                console.log(saveErr)
                return false
            }else{
                return updatedTask
            }
        })
    })
}
