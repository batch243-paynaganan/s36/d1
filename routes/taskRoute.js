const express=require('express');
// create a router instance that functions as a middleware and routing system
const router=express.Router();
const taskController=require("../controller/taskController")
// routes
// this route exprects to receive a GET request at the url "/tasks"
router.get("/",(req,res)=>{
    // invokes the ".getAllTasks" function from the ".taskController" file and send the result to postman
    taskController.getAllTasks().then(
        resultFromController=>res.send(
            resultFromController
        ))
})
// route to create a new task
// this route expects to receive a POST request at the URL "/tasks"
router.post("/", (req,res)=>{
    // if information will be coming from the client side the data can be accessed from the request body
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
    })

// Route to delete a task
// this route expects to receive a delete request at the url "/tasks/:id"
// the colon(:)is an identifier from the URL, it helps create a dynamic route which allows us to supply information in the URL

    router.delete("/:id",(req,res)=>{
        taskController.deleteTask(req.params.id)
        .then(resultFromController=>res.send(resultFromController))
    })

// route to update task
// thsi route expects to receive a PUT request at the url "/tasks/:id"
router.put("/:id",(req,res)=>{
    taskController.updateTask(req.params.id, req.body)
    .then(resultFromController=>res.send(resultFromController))
})
// user module.exports to export the router obj to use in the app.js
module.exports=router;