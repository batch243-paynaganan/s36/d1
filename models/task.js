// create the schema, model, and export the file
// import the mongoose module
const mongoose=require('mongoose');
// create a schema using the mongoose.Schema function
const taskSchema=new mongoose.Schema({
    name: String,
    status:{
        type: String,
        default:"pending"
    }
})

module.exports=mongoose.model("Task",taskSchema);